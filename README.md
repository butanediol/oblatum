# Oblatum图标包

> 这是一个基于[Nano IconPack](https://github.com/by-syk/NanoIconPack)的图标包，现已在[酷安](https://www.coolapk.com/apk/com.oblatum.iconpack)上架。
---
本仓库的内容主要是Oblatum图标包内的图标，并不是图标包程序本身，因此采用BY-CC4.0开源协议。

---
图标文件存放于[这里(192px)](https://github.com/TucaoNico/oblatum/tree/master/app/src/main/res/drawable-nodpi)和[这里(512px)](https://github.com/TucaoNico/oblatum/tree/master/app/src/main/res/mipmap-nodpi)。

如果您有使用这些图标的需求，请与我们取得联系。
- PzHown:1263097055@qq.com
- 直男不懂哲学:n872485238@gmail.com

## LICENSE
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="知识共享许可协议" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />本作品采用<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">知识共享署名 4.0 国际许可协议</a>进行许可。
